/* 
    ACTIVITY 3:

    1. Using the fruits collection, perform the following aggregate stages.
    2. Use the count operator to get the total number of fruits on sale.
    3. Use the count operator to get the total number of fruits with stock more than 20.
    4. Use the average operator to get the average price of the fruits on sale.
    5. Use the max operator to get the highest stock per supplier.
    6. Use the min operator to get the lowest stock per supplier.
    7. Create an "aggregate_act.js" file on where to write and save the solution for the activity.

*/

// =====================================================================


// 2. Use the count operator to get the total number of fruits on sale.

db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$count: "Total numbers of fruits on sale"}
    ]
);

// =====================================================================


// 3. Use the count operator to get the total number of fruits with stock more than 20.

db.fruits.aggregate(
    [
        {$match: {stock: {$gt: 20}}},
        {$count: "Total number of fruits with stock more than 20"}
    ]
);

// =====================================================================


// 4. Use the average operator to get the average price of the fruits on sale.

db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$group: {_id: "$onSale", average_price: {$avg: "$price"}}}
    ]
);

// ====================================================================


// 5. Use the max operator to get the highest stock per supplier.

db.fruits.aggregate(
    [
        {$group: {_id: "$supplier_id", highest_stock_per_supplier: {$max: "$stock"}}},
        {$sort: {highest_stock_per_supplier: -1}}
    ]
);



// 6. Use the min operator to get the lowest stock per supplier.

db.fruits.aggregate(
    [
        {$group: {_id: "$supplier_id", lowest_stock_per_supplier: {$min: "$stock"}}},
        {$sort: {lowest_stock_per_supplier: 1}}
    ]
);