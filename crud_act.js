/* 
    ACTIVITY 1:

    1. Using the course-booking-app database, perform the following CRUD operations.

    2. Insert the following document with the following fields and values:
        name: HTML 101,
        price: 1500,
        slots: 20,
        description: Introduction to HTML,
        isActive: true

        name: CSS 101,
        price: 2500,
        slots: 15,
        description: Introduction to CSS,
        isActive: true

        name: JavaScript 101,
        price: 3500,
        slots: 0,
        description: Introduction to JavaScript,
        isActive: false


*/

    // To insert/add collection in the database
    db.courses.insertMany([
        {
            name: "HTML 101",
            price: 1500,
            slots: 20,
            description: "Introduction to HTML",
            isActive: true
        },
        {
            name: "CSS 101",
            price: 2500,
            slots: 15,
            description: "Introduction to CSS",
            isActive: true
        },
        {
            name: "JavaScript 101",
            price: 3500,
            slots: 0,
            description: "Introduction to JavaScript",
            isActive: false
        }
    ]);

    // To find the inserted collection or documents
    db.courses.find();

// =====================================================================

/* 
    3. Update JavaScript 101 and set its slots to 10 and status to active.

*/

    // To update a document using _id
    db.courses.updateOne(
        { _id: ObjectId("630d6d7ab28c99bb48369387")},
        {
        $set: {
             slots: 10,
            isActive: true
            } 
        }
    );


    // To update a document using one property 
    db.courses.updateOne(
        {name:"JavaScript 101"},
        {
            $set: {
                slots: 10,
                isActive: true
            }
        }
    );

    // To find the updated document
    db.courses.find(
        {name: "JavaScript 101"}
    );
// =================================================================

/*
    4. Add a dummy course document with the following values

        name: Sample,
        price: 0,
        slots: 0,
        description: Sample Introduction,
        isActive: false
        schedule: 2022-08-29T16:00:00Z

*/

    // To insert/add a document in the database
    db.courses.insertOne(
        {
            name: "Sample",
            price: 0,
            slots: 0,
            description: "Sample Introduction",
            isActive: false,
            schedule: "2022-08-29T16:00:00Z"
        }
    );


    // To find the inserted document
    db.courses.find(
        {name: "Sample"}
    );

// ====================================================================



/*
    5. Overwrite the dummy course document with the following values:

        name: MongoDB 101,
        price: 3000,
        slots: 0,
        description: Introduction to Database and NoSQL,
        isActive: false
*/

    // To overwrite fields and value of a document using _id
    db.courses.replaceOne(
        { _id: ObjectId("630d6f6db28c99bb48369388")},
        {
            name: "MongoDB 101",
            price: 3000,
            slots: 0,
            description: "Introduction to Database and NoSQL",
            isActive: false
        }
    );


    // To overwrite fields and value of a document using a property
    db.courses.replaceOne(
        {name: Sample},
        {
            name: "MongoDB 101",
            price: 3000,
            slots: 0,
            description: "Introduction to Database and NoSQL",
            isActive: false
        }
    );


    // To find the updated document
    db.courses.find(
        { _id: ObjectId("630d6f6db28c99bb48369388")}
    );



/*
    6. Remove all inactive course in the course collections.

    7. Create a "crud_act.js" file on where to write and save the solution for the activity.
*/

    // To remove documents in the database
    db.courses.deleteMany(
        {isActive: false}
    );