/* 
    ACTIVITY 2:

    1. Using the course collection, perform the following queries.
    2. Find the courses with the price less than or equal to 3000.
    3. Find all the active course and with slots greater than 25.
    4. Look for all active course that contains letter "s" on their name.
    5. Get all course with slot greater than 10 and price less than or equal to 3000.
    6. Create an "advQueries_act.js" file on where to write and save the solution for the activity.

*/
// ====================================================================


// 2. Find the courses with the price less than or equal to 3000.

db.courses.find(
    {
        price: {$lte: 3000}
    }
);

// ====================================================================



// 3. Find all the active course and with slots greater than 25.

db.courses.find(
    {
        $and: [
            {isActive: true},
            {slots: {$gt: 25}}
        ]
    }
);


// ===================================================================


// 4. Look for all active course that contains letter "s" on their name.

db.courses.find(
    {
        $and: [
            {isActive: true},
            {name: {
                    $regex: "s",
                    $options: '$i'
                }
            }
        ]
    }
);


// =====================================================================


// 5. Get all course with slot greater than 10 and price less than or equal to 3000.

db.courses.find(
    {
        $and: [
            {slots: {$gt: 10}},
            {price: {$lte: 3000}}
        ]
    }
);

